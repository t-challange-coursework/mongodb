FROM mongo:4.2.1
ENV MONGO_INITDB_DATABASE=tchallenge
COPY ./init.js /docker-entrypoint-initdb.d/
EXPOSE 27017
